var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var logger = require('morgan');
var cors = require('cors');

// todo :post
var loginuser = require('./router/post/login');
var register_buyer = require('./router/post/register-buyer');
var register_seller = require('./router/post/register-seller');
var postlocation = require('./router/post/post-location');


var fileRoutes = require('./router/file');


// todo :get
var test = require('./router/get/test');
var getlocation = require('./router/get/get-location');
var recommend = require('./router/get/get-recommend');
var property = require('./router/get/get-pro/get-property');
var member = require('./router/get/get-member');
var profile = require('./router/get/get-profile');
var zone = require('./router/get/get-loc-zone');
var province = require('./router/get/get-loc-province');
var testprovince = require('./router/get/get-test-province');
// var proforprov = require('./router/get/get-pro-for-prov');
var recommainpage = require('./router/get/get-recom-mainpage');

// อสังหา
var house = require('./router/get/get-pro/get-pro-house');
var townhouse = require('./router/get/get-pro/get-pro-townhouse');
var apartment = require('./router/get/get-pro/get-pro-apartment');
var commercial = require('./router/get/get-pro/get-pro-commercial');
var condominium = require('./router/get/get-pro/get-pro-condominium');
var land = require('./router/get/get-pro/get-pro-land');
var prodetail = require('./router/get/get-pro/get-pro-detail');
var promap = require('./router/get/get-pro/get-pro-map');
var protype = require('./router/get/get-pro/get-pro-type');

// ขาย
var sellhouse = require('./router/get/get-sell/get-sell-house');
var selltownhouse = require('./router/get/get-sell/get-sell-townhouse');
var sellapart = require('./router/get/get-sell/get-sell-apart');
var sellcomm = require('./router/get/get-sell/get-sell-comm');
var sellcondo = require('./router/get/get-sell/get-sell-condo');
var sellland = require('./router/get/get-sell/get-sell-land');
// เช่า
var renthouse = require('./router/get/get-rent/get-rent-house');
var renttownhouse = require('./router/get/get-rent/get-rent-townhouse');
var rentapart = require('./router/get/get-rent/get-rent-apt');
var rentcomm = require('./router/get/get-rent/get-rent-comm');
var rentcondo = require('./router/get/get-rent/get-rent-condo');
var rentland = require('./router/get/get-rent/get-rent-land');

var filter = require('./router/get/get-pro/get-pro-filter');
var poppular = require('./router/get/get-pro/get-pro-poppular');

var gettest = require('./router/get/get-test');



// todo :put
var putlocation = require('./router/put/put-location');
var putrecom = require('./router/put/put-recom');
var propublish = require('./router/put/put-status-pro-Publish');
var prodraft = require('./router/put/put-status-pro-draft');
var promodify = require('./router/put/put-status-pro-modify');
var proclose = require('./router/put/put-status-pro-close');
var putimage = require('./router/put/put-image');
var proview = require('./router/put/put-pro-view');


// todo :delete
var deletelocation = require('./router/delete/delete-location');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json({ limit: '10mb', extended: true }))
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }))



//  ****** part ******

// todo :select
app.use('/test', test);
app.use('/getlocation', getlocation);                       // ทำเล
app.use('/recommend', recommend);                           //จัดการรายการแนะนำ
app.use('/property', property);                             //รายการอสังหา
app.use('/member', member);                                 //รายการสมาชิก
app.use('/profile', profile);                               //โปรไฟลสมาชิก
app.use('/house', house);                                   //บ้านเดี่ยว
app.use('/townhouse', townhouse);                           //ทาวน์เฮ้าส์
app.use('/apartment', apartment);                           //อพาร์ทเม้น
app.use('/commercial', commercial);                         //เชิงพาณิชย์
app.use('/condominium', condominium);                         //คอนโดมิเนียม
app.use('/land', land);                                       //ที่ดิน
app.use('/prodetail', prodetail);                             // รายละเอียดอสังหา    
app.use('/promap', promap);                                   // อสังหาจากแผนที่    
app.use('/zone', zone);                                       // ภูมิภาค   
app.use('/province', province);                               // จังหวัด  ยังใช้ไม่ได้ 
app.use('/testprovince', testprovince);                       // จังหวัด  
// app.use('/proforprov', proforprov);                        // อสังหา จาก จังหวัด  
app.use('/protype', protype);                                 // อสังหา จาก ประเภท  
app.use('/recommainpage', recommainpage);                      // รายการแนะนำ  

app.use('/sellhouse', sellhouse);                               // รายการ ขาย  
app.use('/selltownhouse', selltownhouse);                      // รายการ ขาย  
app.use('/sellapart', sellapart);                              // รายการ ขาย  
app.use('/sellcomm', sellcomm);                                 // รายการ ขาย  
app.use('/sellcondo', sellcondo);                               // รายการ ขาย  
app.use('/sellland', sellland);                                 // รายการ ขาย  

app.use('/renthouse', renthouse);                               // รายการ เช่า  
app.use('/renttownhouse', renttownhouse);                      // รายการ เช่า  
app.use('/rentapart', rentapart);                              // รายการ เช่า  
app.use('/rentcomm', rentcomm);                                 // รายการ เช่า  
app.use('/rentcondo', rentcondo);                               // รายการ เช่า  
app.use('/rentland', rentland);                                 // รายการ เช่า  

app.use('/filter', filter);                                 // รายการ filter  
app.use('/poppular', poppular);                                 // รายการ ยอดนืยม


app.use('/gettest', gettest);                     // test





//  todo :update 
app.use('/putlocation', putlocation);           // ทำเล
app.use('/putrecom', putrecom);                 //รายการแนะนำ
app.use('/propublish', propublish);                 //สถานะ อสังหา เผยแพร่
app.use('/prodraft', prodraft);                 //สถานะ อสังหา ร่าง
app.use('/promodify', promodify);                 //สถานะ อสังหา แก้ไข
app.use('/proclose', proclose);                 //สถานะ อสังหา ปิดประกาศ
app.use('/putimage', putimage);                 // รูป
app.use('/proview', proview);                 // อัพยอดวิว


// todo :insert - post
app.use('/login', loginuser);
app.use('/register_buyer', register_buyer);              // สมัครสมาชิก ซื้อ
app.use('/register_seller', register_seller);            // สมัครสมาชิก ขาย
app.use('/postlocation', postlocation);                 // เพิ่มทำเล


app.use('/file', fileRoutes);              //ลอง 


// delete
app.use('/deletelocation', deletelocation);









// end part ^^

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
