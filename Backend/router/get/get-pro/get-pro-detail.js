var express = require('express');
var router = express.Router();
var prodetail = require('../../../service/select/get-pro/get-pro-detail');

router.get('/:pro_id', function (req, res, next) {
    // var data=req.body
    var data = req.params.pro_id
    prodetail.prodetail(data, (err, row) => {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }


    })
});
module.exports = router;