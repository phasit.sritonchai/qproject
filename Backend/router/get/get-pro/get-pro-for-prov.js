var express = require('express');
var router = express.Router();
var proforprov = require('../../../service/select/get-pro/get-pro-for-prov');

router.get('/:location_id', function (req, res, next) {
    // var data=req.body
    var data = req.params.location_id
    proforprov.proforprov(data, (err, row) => {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }


    })
});
module.exports = router;