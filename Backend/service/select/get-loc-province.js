var db = require('../../dbcon');
var province = {
    province: function (data, callback) {
        var sql = `SELECT location.loc_name ,loc_zone
        FROM
        location
        WHERE loc_zone = ?
        ORDER BY loc_name ASC`
        var loc_zone = data;

        return db.query(sql, [loc_zone], callback);
    }

}
module.exports = province;