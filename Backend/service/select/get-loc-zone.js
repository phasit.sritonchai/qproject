var db = require('../../dbcon');
var location = {
    location: function (callback) {
        var sql = `SELECT DISTINCT location.loc_zone 
        FROM
        location
        ORDER BY loc_zone ASC
        `
        return db.query(sql, callback);
    }

}
module.exports = location;