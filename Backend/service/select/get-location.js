var db = require('../../dbcon');
var location = {
    location: function (callback) {
        var sql = `SELECT
        location.location_id,
        location.loc_pic,
        location.loc_name,
        location.loc_zone,
        (SELECT COUNT(location_id) FROM property  WHERE location_id = location.location_id) AS countLoc
        FROM
        location
        ORDER BY
        countLoc DESC,
        location.loc_name ASC
        `
        return db.query(sql, callback);
    }

}
module.exports = location;