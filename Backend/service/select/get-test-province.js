var db = require('../../dbcon');
var province = {
    province: function (callback) {
        var sql = `SELECT DISTINCT loc_zone ,loc_name
        FROM
        location
        ORDER BY loc_name ASC
        `
        return db.query(sql, callback);
    }

}
module.exports = province;