import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  ngOnInit(): void {
    this.router.navigate([{outlet: {primary: 'mainpage',}}])
  }
  
  
  title = 'Property';
  constructor(private router:Router){

  }
}
