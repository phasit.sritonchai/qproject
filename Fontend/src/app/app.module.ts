import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatNativeDateModule } from '@angular/material';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { ServerService } from './@service/server.service';
import { NgbModule, NgbPaginationModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { SlideshowModule } from 'ng-simple-slideshow';
import { CarouselModule } from 'ngx-bootstrap';
import { PapaParseModule } from 'ngx-papaparse';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './mainpage/home/home.component';
import { MainpageComponent } from './mainpage/mainpage.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MainpageComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule,
    NgbPaginationModule,
    NgbAlertModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    SlideshowModule,
    AngularFontAwesomeModule,
    PapaParseModule,
    NgxWebstorageModule.forRoot(),
    CarouselModule.forRoot(),
    
    
  ],
  providers: [ServerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
