import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainpageRoutingModule } from './mainpage-routing.module';
import { HomeComponent } from './home/home.component';
import { MainpageComponent } from './mainpage.component';


@NgModule({
  declarations: [HomeComponent, MainpageComponent],
  imports: [
    CommonModule,
    MainpageRoutingModule
  ]
})
export class MainpageModule { }
