import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { urlServer } from '../@URL/URL';

@Injectable({
  providedIn: 'root'
})
export class ServerService {



  constructor(private http: HttpClient) { }
  // login 
  getLogin(data) {
    return this.http.post(urlServer.ipServer + 'login', data)
  }
  // register-buyer
  onRegisterBuy(data) {
    return this.http.post(urlServer.ipServer + 'register_buyer', data)
  }
  // register-sell
  onRegisterSell(data) {
    return this.http.post(urlServer.ipServer + 'register_seller', data)
  }

  // todo: Get --------------------------//

  // member
  getMember() {
    return this.http.get(urlServer.ipServer + 'member')
  }

  // test
  getTest() {
    return this.http.get(urlServer.ipServer + 'test')
  }

  // get Location
  getLocation() {
    return this.http.get(urlServer.ipServer + 'getlocation')
  }

  // รายการแนะนำ
  getRecommend() {
    return this.http.get(urlServer.ipServer + 'recommend')
  }
  // รายการแนะนำ
  getRecommainpage() {
    return this.http.get(urlServer.ipServer + 'recommainpage')
  }
  // อสังหา
  getProperty() {
    return this.http.get(urlServer.ipServer + 'property')
  }
  // บ้าน
  getHouse() {
    return this.http.get(urlServer.ipServer + 'house')
  }
  // ทาวเฮาส์
  getTownhouse() {
    return this.http.get(urlServer.ipServer + 'townhouse')
  }
  // อพาสเมน
  getApartment() {
    return this.http.get(urlServer.ipServer + 'apartment')
  }
  // อาคารพานิช
  getCommercial() {
    return this.http.get(urlServer.ipServer + 'commercial')
  }
  // คอนโด
  getCondominium() {
    return this.http.get(urlServer.ipServer + 'condominium')
  }
  // ที่ดิน
  getLand() {
    return this.http.get(urlServer.ipServer + 'land')
  }
  // todo : ขาย
  // ขายบ้าน
  getSellHouse() {
    return this.http.get(urlServer.ipServer + 'sellhouse')
  }
  // ขายทาวเฮาส์
  getSellTownhouse() {
    return this.http.get(urlServer.ipServer + 'selltownhouse')
  }
  // ขายอพาสเมน
  getSellApartment() {
    return this.http.get(urlServer.ipServer + 'sellapart')
  }
  // ขายอาคารพานิช
  getSellCommercial() {
    return this.http.get(urlServer.ipServer + 'sellcomm')
  }
  // ขายคอนโด
  getSellCondominium() {
    return this.http.get(urlServer.ipServer + 'sellcondo')
  }
  // ขายที่ดิน
  getSellLand() {
    return this.http.get(urlServer.ipServer + 'sellland')
  }
  // todo : เช่า
  // เช่าบ้าน 
  getRentHouse() {
    return this.http.get(urlServer.ipServer + 'renthouse')
  }
  // เช่าทาวเฮาส์
  getRentTownhouse() {
    return this.http.get(urlServer.ipServer + 'renttownhouse')
  }
  // เช่าอพาสเมน
  getRentApartment() {
    return this.http.get(urlServer.ipServer + 'rentapart')
  }
  // เช่าอาคารพานิช
  getRentCommercial() {
    return this.http.get(urlServer.ipServer + 'rentcomme')
  }
  // เช่าคอนโด
  getRentCondominium() {
    return this.http.get(urlServer.ipServer + 'rentcondo')
  }
  // เช่าที่ดิน
  getRentLand() {
    return this.http.get(urlServer.ipServer + 'rentlande')
  }
  // รายละเอียดสมาชิก
  getProfile(data) {
    return this.http.get(urlServer.ipServer + 'profile/' + data)
  }
  // รายละเอียดสมาชิก
  getProDetail(data) {
    return this.http.get(urlServer.ipServer + 'prodetail/' + data)
  }
  // ภาค
  getZone() {
    return this.http.get(urlServer.ipServer + 'zone')
  }
  // จังหวัด ลองไม้ได้แก้
  getTestProvince() {
    return this.http.get(urlServer.ipServer + 'testprovince')
  }
  // อสังหา จาก จังหวัด 
  getProtype(data) {
    return this.http.get(urlServer.ipServer + 'protype/' + data)
  }
  // รายการยอดนิยม
  getPoppular(data) {
    return this.http.get(urlServer.ipServer + 'poppular')
  }

  // ! END ^ Get -------------------------//

  // todo: Post --------------------------//

  // post location
  postLocation(data) {
    return this.http.post(urlServer.ipServer + 'postlocation', data)
  }


  // ! END ^ Post -------------------------//



  // todo: Update --------------------------//

  // update loc
  onLocation(data) {
    return this.http.put(urlServer.ipServer + 'putlocation', data)
  }

  // update recom
  onRecom(data) {
    return this.http.put(urlServer.ipServer + 'putrecom', data)
  }
  // update สถานะอสังหา
  putPropublish(data) {
    return this.http.put(urlServer.ipServer + 'propublish', data)
  }
  putProdraft(data) {
    return this.http.put(urlServer.ipServer + 'prodraft', data)
  }
  putPromodify(data) {
    return this.http.put(urlServer.ipServer + 'promodify', data)
  }
  putProclose(data) {
    return this.http.put(urlServer.ipServer + 'proclose', data)
  }
  // อัพยอดวิว
  putProview(data) {
    return this.http.put(urlServer.ipServer + 'proview', data)
  }

  // รูป
  putImage(finalJson) {
    return this.http.put(urlServer.ipServer + 'putimage', finalJson)
  }
  // รูป ลอง
  putImagetest(formData) {
    return this.http.put(urlServer.ipServer + 'putimage', formData)
  }


  // ! END ^ Update -------------------------//



  // todo: Delete --------------------------//

  // delete loc
  deleteLocation(data) {
    return this.http.delete(urlServer.ipServer + 'deletelocation/' + data)
  }



  // ! END ^ Delete -------------------------//








  // // อสังหา แผนที่
  // getMpaProperty() {
  //   return this.http.get(urlServer.ipServer + 'promap')
  // }


}
