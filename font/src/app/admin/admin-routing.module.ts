import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AdminhomeComponent } from './adminhome/adminhome.component';
import { ManageSaleComponent } from './manage-sale/manage-sale.component';
import { ManageMemberComponent } from './manage-member/manage-member.component';
import { WriteAdsComponent } from './write-ads/write-ads.component';
import { AdsComponent } from './ads/ads.component';
import { ChatComponent } from './chat/chat.component';
import { ManageMainpageComponent } from './manage-mainpage/manage-mainpage.component';
import { ManageLocationComponent } from './manage-location/manage-location.component';
import { ProfileComponent } from './profile/profile.component';
import { ProDetailComponent } from './manage-sale/pro-detail/pro-detail.component';
import { MemberDetailComponent } from './manage-member/member-detail/member-detail.component';
import { RecommendComponent } from './manage-mainpage/recommend/recommend.component';
import { SearchMapComponent } from './search/search-map/search-map.component';
import { MeasureComponent } from './measure/measure.component';

const routes: Routes = [
  {
    path: 'admin',
    component: AdminComponent,
    children: [
      {
        path: 'adminhome',
        component: AdminhomeComponent
      },
      {
        path: 'managesale',    // รายการอสังหา
        component: ManageSaleComponent,
      },
      {
        path: 'prodetail/:pro_id',  //รายละเอียดอสังหา
        component: ProDetailComponent
      },
      {
        path: 'managemember',       // รายการสมาชิก
        component: ManageMemberComponent
      },
      {
        path: 'memberdetail/:email_id',       // รายละเอียดสมาชิก
        component: MemberDetailComponent
      },
      {
        path: 'writerads',        // เขียนโฆษณา
        component: WriteAdsComponent
      },
      {
        path: 'ads',            // โฆษณา
        component: AdsComponent
      },
      {
        path: 'chat',      // แชท
        component: ChatComponent
      },
      {
        path: 'managemainpage',       //รายการแนะนำ
        component: ManageMainpageComponent
      },
      {
        path: 'recommend/:recom_id',       //เพิ่มรายการแนะนำ
        component: RecommendComponent
      },
      {
        path: 'managelocation',     //จัดการทำเล
        component: ManageLocationComponent
      },
      {
        path: 'profile',
        component: ProfileComponent
      },
      {
        path: 'searchmap',            // ค้นหาจากแผนที่
        component: SearchMapComponent
      },
      {
        path: 'measure',            // วัดขนาดจากแผนที่
        component: MeasureComponent
      },
    ]
  },
  {
    path: '',
    redirectTo: 'admin/managesale',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
