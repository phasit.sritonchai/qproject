import { Component, OnInit } from '@angular/core';
import { SessionService } from '../@service/session.service';
import { Router } from '@angular/router';
import { ServerService } from '../@service/server.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  user: any;

  constructor(
    private session: SessionService,
    private route: Router,
    private service: ServerService,
    
  ) { }

  ngOnInit() {
   
    this.user = this.session.getActiveUser();
    console.log(this.user);


    

    
  }

  
  onLogout() {
    this.session.clearActiveUser();
    this.route.navigate(['/mainpage/mainpage/home'])
  }


  

}
