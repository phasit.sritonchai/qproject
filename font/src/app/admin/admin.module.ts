import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { AdminhomeComponent } from './adminhome/adminhome.component';
import { MaterialModule } from '../material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatNativeDateModule } from '@angular/material';
import { SliderModule } from 'angular-image-slider';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { CarouselModule } from 'ngx-bootstrap';
import { ManageSaleComponent } from './manage-sale/manage-sale.component';
import { ManageMemberComponent } from './manage-member/manage-member.component';
import { WriteAdsComponent } from './write-ads/write-ads.component';
import { AdsComponent } from './ads/ads.component';
import { ChatComponent } from './chat/chat.component';
import { ManageMainpageComponent } from './manage-mainpage/manage-mainpage.component';
import { ManageLocationComponent } from './manage-location/manage-location.component';
import { ProfileComponent } from './profile/profile.component';
import { ProDetailComponent } from './manage-sale/pro-detail/pro-detail.component';
import { MemberDetailComponent } from './manage-member/member-detail/member-detail.component';
import { RecommendComponent } from './manage-mainpage/recommend/recommend.component';
import { SearchComponent } from './search/search.component';
import { SearchMapComponent } from './search/search-map/search-map.component';
import { MeasureComponent } from './measure/measure.component';
import { AgmCoreModule } from '@agm/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import {FileUploadModule} from 'ng2-file-upload';




@NgModule({
  declarations: [AdminComponent,
    AdminhomeComponent,
    ManageSaleComponent,
    ManageMemberComponent,
    WriteAdsComponent, 
    AdsComponent,
    ChatComponent,
    ManageMainpageComponent,
    ManageLocationComponent,
    ProfileComponent,
    ProDetailComponent,
    MemberDetailComponent,
    RecommendComponent,
    SearchComponent,
    SearchMapComponent,
    MeasureComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    SliderModule,
    AngularFontAwesomeModule,
    NgbModule.forRoot(), //
    CarouselModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDJXSMouDkOYbIfcrzyXLNQpXCCn-_vAaU'}  //google api
    ),
    // FileUploadModule,
    
    
    
  ]
})
export class AdminModule { }
