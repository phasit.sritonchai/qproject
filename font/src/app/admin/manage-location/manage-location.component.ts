import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator, MatDialog } from '@angular/material';
import { ServerService } from 'src/app/@service/server.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { delay } from 'q';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-manage-location',
  templateUrl: './manage-location.component.html',
  styleUrls: ['./manage-location.component.scss']
})
export class ManageLocationComponent implements OnInit {
  displayedColumns: string[] = [ 'loc_name', 'loc_zone', 'countLoc', 'edit', 'delete'];
  dataSource: MatTableDataSource<[any]>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('success') success: ElementRef;
  loc_name;
  loc_pic;

  public AddLocation = new FormGroup({
    loc_name: new FormControl(''),
    loc_pic: new FormControl(''),

  })

  public updateLoc = new FormGroup({
    loc_name: new FormControl(''),
    loc_pic: new FormControl(''),

  })
  location_id: any;

  constructor(
    private service: ServerService,
    private dialog: MatDialog,
    private modalService: NgbModal,
    private modal: NgbModal,

  ) { }

  ngOnInit() {
    this.getTable();
  }
  getTable() {
    this.service.getLocation().subscribe(
      (res) => {
        this.dataSource = new MatTableDataSource(res as any[]);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    )
  }

  // ตัวกรอง
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  closeModal() {
    this.modalService.dismissAll();
  }

  openModalEditLoc(data, modal) {
    console.log(data);
    this.loc_name = data.loc_name;
    this.loc_pic = data.loc_pic;
    this.location_id = data.location_id;

    this.modal.open(modal, { centered: true })
  }



  onUpdateLoc() {
    console.log(this.updateLoc.value)
    const data = {
      loc_name: this.loc_name,
      loc_pic: this.loc_name,
      location_id: this.location_id
    }
    console.log(data)
    this.service.onLocation(data).subscribe(
      async (res) => {
        this.modalService.open(this.success)
        this.getTable();
        await delay(1000);
        this.closeModal();

      }
    )
  }
  // onUpdateLoc() {
  //   console.log(this.updateLoc.value)
  //   this.service.onLocation(this.updateLoc.value).subscribe(
  //     async (res) => {
  //       this.modalService.open(this.success)
  //       this.getTable();
  //       await delay(1000);
  //       this.closeModal();

  //     }
  //   )
  // }
  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  // Modal Login
  openModalAddloc(modal, data) {

    this.modalService.open(modal, { centered: true })
  }

  onModalDelete(data, modal) {
    this.location_id = data.location_id
    this.modalService.open(modal, { centered: true })
  }

  onDeleteLoc() {
    this.service.deleteLocation(this.location_id).subscribe(
      async (res) => {
        this.modalService.open(this.success)
        this.getTable();
        await delay(1000);
        this.closeModal();
      }
    )
  }
  onAddlocation() {
    // const data = {
    //   loc_name: this.loc_name,
    //   loc_pic: this.loc_pic
    // }
    // console.log(data);
    // console.log(this.AddLocation)

    this.service.postLocation(this.AddLocation.value).subscribe(
      async (res) => {
        
          // this.dialog.open(AlertAddUserSuccess)
          this.getTable();
          this.closeModal();
          
        }
      
      
    )
  }
  // onRegister() {
  //   // console.log(this.registerForm.value)
  //   const data = {
  //     fname: this.Fname,
  //     lname: this.Lname,
  //     email_id: this.EmailRegis,
  //     password: this.Password,
  //     repassword: this.Repassword,
  //     phone: this.Phone,
  //     id_line: this.Id_line
  //   }
  //   console.log(data)//if  ทุก คอลัมไม่เท่ากับว่าง

  //     this.service.onRegisterSell(data).subscribe(

  //       async (res) => {
  //         this.modalService.open(this.success)
  //         await delay(1000);
  //         this.modalService.dismissAll();
  //         window.history.go(0);
  //       },


  //     )
  //   } 


}
