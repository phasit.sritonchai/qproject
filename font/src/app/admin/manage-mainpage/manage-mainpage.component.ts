import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatSort, MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { ServerService } from 'src/app/@service/server.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { delay } from 'q';

@Component({
  selector: 'app-manage-mainpage',
  templateUrl: './manage-mainpage.component.html',
  styleUrls: ['./manage-mainpage.component.scss']
})
export class ManageMainpageComponent implements OnInit {
  displayedColumns1: string[] = ['a', 'b', 'c', 'd'];
  dataSource1: MatTableDataSource<[any]>;

  // displayedColumns2: string[] = ['e', 'f', 'g', 'h'];
  // dataSource2: MatTableDataSource<[any]>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  // @ViewChild(MatSort) sort1: MatSort;
  @ViewChild('success') success: ElementRef;
  recom_id: any;
  pro_id: any;
  constructor(
    private service: ServerService,
    private dialog: MatDialog,
    private modalService: NgbModal,
    private modal: NgbModal
  ) { }

  ngOnInit() {
    this.getTable();
  }

  getTable() {
    this.service.getRecommend().subscribe(
      (res) => {
        this.dataSource1 = new MatTableDataSource(res as any[]);
        this.dataSource1.sort = this.sort;
        this.dataSource1.paginator = this.paginator;
      }
    )
  }
  // ตัวกรอง
  applyFilter(filterValue: string) {
    this.dataSource1.filter = filterValue.trim().toLowerCase();
    if (this.dataSource1.paginator) {
      this.dataSource1.paginator.firstPage();
    }
  }
  closeModal() {
    this.modalService.dismissAll();
  }

  // // Modal recom
  // openModalAddRecom(modal, data) {
  //   this.getTableProperty();
  //   this.modalService.open(modal, { centered: true, size: "lg" })
  // }

  // getTableProperty() {
  //   this.service.getProperty().subscribe(
  //     (res) => {
  //       this.dataSource2 = new MatTableDataSource(res as any[]);
  //       this.dataSource2.sort = this.sort1;
  //       this.dataSource2.paginator = this.paginator;
  //     }
  //   )
  // }
  // ตัวกรอง
  // applyFilter2(filterValue: string) {
  //   this.dataSource2.filter = filterValue.trim().toLowerCase();
  //   if (this.dataSource2.paginator) {
  //     this.dataSource2.paginator.firstPage();
  //   }
  // }

  // onUpdateRecom() {
  //   const data = {
  //     recom_id: this.recom_id,
  //     pro_id: this.pro_id
  //   }
    
  //   console.log(data)
  //   this.service.onRecom(data).subscribe(
  //     async (res) => {
  //       this.modalService.open(this.success)
  //       this.getTable();
  //       await delay(1000);
  //       this.closeModal();

  //     }
  //   )
  // }
}
