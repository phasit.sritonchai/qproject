import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServerService } from 'src/app/@service/server.service';
import { MatDialog, MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-member-detail',
  templateUrl: './member-detail.component.html',
  styleUrls: ['./member-detail.component.scss']
})
export class MemberDetailComponent implements OnInit {
  displayedColumns: string[] = ['a', 'b' , 'c', 'd'];
  dataSource: MatTableDataSource<[any]>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  id;
  data;
  private sub: any;
  email_id: any;
  fname;
  lname;
  id_line;
  facebook;
  profile_pic;
  cus_detail;
  cus_status;
  phone;
  constructor(
    private service: ServerService,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private modalService: NgbModal,
    private modal: NgbModal
  ) { }

  ngOnInit() {

    this.data = this.route.snapshot.paramMap.getAll('email_id');
   
    // console.log(this.id)
    console.log(this.data)

    this.service.getProfile(this.data).subscribe(
      (res) => {
        this.email_id = res[0].email_id,
        this.fname = res[0].fname,
        this.lname = res[0].lname,
        this.id_line = res[0].id_line,
        this.facebook = res[0].facebook,
        this.profile_pic = res[0].profile_pic,
        this.cus_detail = res[0].cus_detail,
        this.cus_status = res[0].cus_status,
        this.phone = res[0].phone
      }
    )

 

  }

 


}
