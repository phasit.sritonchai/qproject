import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainpageComponent } from './mainpage.component';
import { HomeComponent } from './home/home.component';
import { MapsizeComponent } from './mapsize/mapsize.component';
import { AdvertiseComponent } from './advertise/advertise.component';
import { MapComponent } from './search/map/map.component';
import { LocationComponent } from './search/location/location.component';
import { PropertyComponent } from './product/property/property.component';
import { HouseComponent } from './typesell/sell/house/house.component';
import { TownhouseComponent } from './typesell/sell/townhouse/townhouse.component';
import { ApartmentComponent } from './typesell/sell/apartment/apartment.component';
import { CommercialComponent } from './typesell/sell/commercial/commercial.component';
import { CondominiumComponent } from './typesell/sell/condominium/condominium.component';
import { LandComponent } from './typesell/sell/land/land.component';
import { RenthouseComponent } from './typesell/rent/renthouse/renthouse.component';
import { RenttownhouseComponent } from './typesell/rent/renttownhouse/renttownhouse.component';
import { RentapartmentComponent } from './typesell/rent/rentapartment/rentapartment.component';
import { RentcommercialComponent } from './typesell/rent/rentcommercial/rentcommercial.component';
import { RentcondominiumComponent } from './typesell/rent/rentcondominium/rentcondominium.component';
import { RentlandComponent } from './typesell/rent/rentland/rentland.component';
import { FilterComponent } from './search/filter/filter.component';

const routes: Routes = [
  {
    path: 'mainpage',
    component: MainpageComponent,
    children: [
      { // หน้าหลัก
        path: 'home',
        component: HomeComponent
      },

      // todo : ขาย
      { // ขาย บ้าน
        path: 'sellhouse',
        component: HouseComponent
      },
      { // ขาย ทาวน์เฮ้าส์
        path: 'selltownhouse',
        component: TownhouseComponent
      },
      { // ขาย อพาร์ทเม้น
        path: 'sellapartment',
        component: ApartmentComponent
      },
      { // ขาย เชิงพาณิชย์
        path: 'sellcommercial',
        component: CommercialComponent
      },
      { // ขาย คอนโดมิเนียม
        path: 'sellcondominium',
        component: CondominiumComponent
      },
      { // ขาย ที่ดิน
        path: 'sellland',
        component: LandComponent
      },

      // todo : เช่า
      { // เช่า บ้าน
        path: 'renthouse',
        component: RenthouseComponent
      },
      { // เช่า ทาวน์เฮ้าส์
        path: 'renttownhouse',
        component: RenttownhouseComponent
      },
      { // เช่า อพาร์ทเม้น
        path: 'rentapartment',
        component: RentapartmentComponent
      },
      { // เช่า เชิงพาณิชย์
        path: 'rentcommercial',
        component: RentcommercialComponent
      },
      { // เช่า คอนโดมิเนียม
        path: 'rentcondominium',
        component: RentcondominiumComponent
      },
      { // เช่า ที่ดิน
        path: 'rentland',
        component: RentlandComponent
      },

      // todo: ค้นหา
      { // ค้นหาจากแผนที่
        path: 'map',
        component: MapComponent
      },
      { // ค้นหาจากที่ตั้ง
        path: 'location',
        component: LocationComponent
      },
      { // ค้นหา fillter
        path: 'fillter',
        component: FilterComponent
      },

      // todo : รายละเอียด
      { // รายละเอียด
        path: 'property/:pro_id',
        component: PropertyComponent
      },


      // todo : รายละเอียด
      { // วัดขนาดที่ดิน
        path: 'mapsize',
        component: MapsizeComponent
      },

      // โฆษณา
      {
        path: 'advertise',
        component: AdvertiseComponent
      },
    ]
  },
  {
    path: '',
    redirectTo: 'mainpage',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainpageRoutingModule { }
