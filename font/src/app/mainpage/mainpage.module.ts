import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainpageRoutingModule } from './mainpage-routing.module';
import { MainpageComponent } from './mainpage.component';
import { HomeComponent } from './home/home.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatNativeDateModule } from '@angular/material';
import { SliderModule } from 'angular-image-slider';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { CarouselModule } from 'ngx-bootstrap';
import { MaterialModule } from '../material';
import { MapsizeComponent } from './mapsize/mapsize.component';
import { AdvertiseComponent } from './advertise/advertise.component';
import { AgmCoreModule } from '@agm/core';
import { MapComponent } from './search/map/map.component';
import { LocationComponent } from './search/location/location.component';
import { PropertyComponent } from './product/property/property.component';
import { HouseComponent } from './typesell/sell/house/house.component';
import { TownhouseComponent } from './typesell/sell/townhouse/townhouse.component';
import { ApartmentComponent } from './typesell/sell/apartment/apartment.component';
import { CommercialComponent } from './typesell/sell/commercial/commercial.component';
import { CondominiumComponent } from './typesell/sell/condominium/condominium.component';
import { LandComponent } from './typesell/sell/land/land.component';
import { RenthouseComponent } from './typesell/rent/renthouse/renthouse.component';
import { RenttownhouseComponent } from './typesell/rent/renttownhouse/renttownhouse.component';
import { RentapartmentComponent } from './typesell/rent/rentapartment/rentapartment.component';
import { RentcommercialComponent } from './typesell/rent/rentcommercial/rentcommercial.component';
import { RentcondominiumComponent } from './typesell/rent/rentcondominium/rentcondominium.component';
import { RentlandComponent } from './typesell/rent/rentland/rentland.component';
import { FilterComponent } from './search/filter/filter.component';


@NgModule({
  declarations: [
    MainpageComponent,
    HomeComponent, 
    MapsizeComponent, 
    AdvertiseComponent, 
    MapComponent, 
    LocationComponent, 
    PropertyComponent, 
    HouseComponent, 
    TownhouseComponent, 
    ApartmentComponent, 
    CommercialComponent, 
    CondominiumComponent, 
    LandComponent, 
    RenthouseComponent, 
    RenttownhouseComponent, 
    RentapartmentComponent, 
    RentcommercialComponent, 
    RentcondominiumComponent, 
    RentlandComponent, FilterComponent
  ],

  imports: [
    CommonModule,
    MainpageRoutingModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    SliderModule,
    AngularFontAwesomeModule,
    CarouselModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDJXSMouDkOYbIfcrzyXLNQpXCCn-_vAaU',
      libraries: ['places','geometry','drawing']
    }),//google api

    
  ]
})
export class MainpageModule { }
