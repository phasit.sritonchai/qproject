import { Component, OnInit } from '@angular/core';
import { google } from '@agm/core/services/google-maps-types';
import { ServerService } from 'src/app/@service/server.service';
import { MatDialog } from '@angular/material';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-mapsize',
  templateUrl: './mapsize.component.html',
  styleUrls: ['./mapsize.component.scss']
})
export class MapsizeComponent implements OnInit {

  lat2: number;
  lng2: number;


 

  constructor(
    private service: ServerService,
    private dialog: MatDialog,
    private modalService: NgbModal,
    private modal: NgbModal,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getUserLocation();
 
  }



  // ที่อยู่ผู้ใช้
  private getUserLocation() {
    if (navigator.geolocation) {
       navigator.geolocation.getCurrentPosition(position => {
        this.lat2 = position.coords.latitude;
        this.lng2 = position.coords.longitude;

      });
    }
  }
  



  
}

  




