import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { ServerService } from 'src/app/@service/server.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {
  displayedColumns: string[] = ['a', 'b', 'c', 'd', 'e'];
  dataSource: MatTableDataSource<[any]>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  roomm = new FormControl('');
  rooms;
  province;
  Province = new FormControl('');

  //How do I filter based on selected value? 
  search(selectedValue: string, selectedValueEle: string) {
    selectedValue = selectedValue.trim(); // Remove whitespace
    selectedValue = selectedValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = selectedValue || selectedValueEle;
   
  }

  
  constructor(
    private service: ServerService,
    private dialog: MatDialog,
    private modalService: NgbModal,
    private modal: NgbModal,

  ) { }

  ngOnInit() {
    this.getLoc();
    this.getProvince();
    this.getTablepro();
    // console.log(this.Province);
  }

  getLoc() {
    this.service.getZone().subscribe(
      (res) => {
        // console.log(res);
        this.rooms = res;
      }
    )
  }

  getProvince() {
    this.service.getTestProvince().subscribe(
      (res) => {
        // console.log(res);
        this.province = res;
      }
    )
  }

  // ตัวกรอง
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getTablepro() {
    this.service.getProperty().subscribe(
      (res) => {
        this.dataSource = new MatTableDataSource(res as any[]);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    )
  }

}
