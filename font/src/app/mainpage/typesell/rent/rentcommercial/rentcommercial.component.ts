import { Component, OnInit } from '@angular/core';
import { ServerService } from 'src/app/@service/server.service';
import { MatDialog } from '@angular/material';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-rentcommercial',
  templateUrl: './rentcommercial.component.html',
  styleUrls: ['./rentcommercial.component.scss']
})
export class RentcommercialComponent implements OnInit {
  products: Object;
 

  constructor(
    private service: ServerService,
    private dialog: MatDialog,
    private modalService: NgbModal,
  ) { }

  ngOnInit() {
    
    this.getProperty();
  }



  getProperty() {
    this.service.getRentCommercial().subscribe(
      (res) => {
        console.log(res);

        this.products = res;

      })
  }

}
