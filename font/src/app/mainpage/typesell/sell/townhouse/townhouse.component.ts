import { Component, OnInit } from '@angular/core';
import { ServerService } from 'src/app/@service/server.service';
import { MatDialog } from '@angular/material';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-townhouse',
  templateUrl: './townhouse.component.html',
  styleUrls: ['./townhouse.component.scss']
})
export class TownhouseComponent implements OnInit {
  products: Object;
 

  constructor(
    private service: ServerService,
    private dialog: MatDialog,
    private modalService: NgbModal,
  ) { }

  ngOnInit() {
    
    this.getProperty();
  }



  getProperty() {
    this.service.getSellTownhouse().subscribe(
      (res) => {
        console.log(res);

        this.products = res;

      })
  }

}
