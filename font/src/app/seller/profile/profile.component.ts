import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/@service/session.service';
import { Router } from '@angular/router';
import { ServerService } from 'src/app/@service/server.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: any;

  // public updateprofile = new FormGroup({
  //   fname: new FormControl(''),
  //   lname: new FormControl(''),
    // phone: new FormControl(''),
    // id_line: new FormControl(''),
    // facebook: new FormControl(''),
    // cus_detail: new FormControl(''),
  // })
  fname;
  email_id;
  lname;
  phone;
  id_line;
  facebook;
  cus_detail;
  constructor(
    private session: SessionService,
    private route: Router,
    private service: ServerService,
    private modalService: NgbModal,
    private modal: NgbModal,

  ) { }

  ngOnInit() {
    this.user = this.session.getActiveUser();
    console.log(this.user);
    this.service.getProfile(this.user[0].email_id).subscribe(
      (res) => {
        // console.log(res);
        this.email_id = res[0].email_id;
        this.fname = res[0].fname;
        this.lname = res[0].lname;
        this.phone = res[0].phone;
        this.id_line = res[0].email;
        this.facebook = res[0].facebook;
        this.cus_detail = res[0].cus_detail;
    
      }
    )
    // console.log(this.user);
  }

  closeModal() {
    this.modalService.dismissAll();
  }

  // Modal recom
  // openModalProfile(modal, user) {
  //   // this.fname = user.fname[0];
  //   console.log(this.user);
    
  //   this.modalService.open(modal, { centered: true,})
  // }

}
