import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SellerComponent } from './seller.component';
import { SellerhomeComponent } from './sellerhome/sellerhome.component';
import { SellPropertyComponent } from './sell-property/sell-property.component';
import { SellChatComponent } from './sell-chat/sell-chat.component';
import { SellingComponent } from './selling/selling.component';
import { ProfileComponent } from './profile/profile.component';


const routes: Routes = [
  {
    path:'seller',
    component:SellerComponent,
    children:[
      {
        path:'sellerhome',
        component:SellerhomeComponent
      },
      {
        path:'profile',
        component:ProfileComponent
      },
      {
        path:'selle-property',
        component:SellPropertyComponent
      },
      {
        path:'seller-chat',
        component:SellChatComponent
      },
      {
        path:'selling',
        component:SellingComponent
      },
    ]
  },
  {
    path:'',
    redirectTo:'seller/sellerhome',
    pathMatch:'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SellerRoutingModule { }
