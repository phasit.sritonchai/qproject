import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SellerRoutingModule } from './seller-routing.module';
import { SellerComponent } from './seller.component';
import { SellerhomeComponent } from './sellerhome/sellerhome.component';
import { MaterialModule } from '../material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatNativeDateModule } from '@angular/material';
import { SliderModule } from 'angular-image-slider';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { CarouselModule } from 'ngx-bootstrap';
import { ProfileComponent } from './profile/profile.component';
import { SellPropertyComponent } from './sell-property/sell-property.component';
import { SellChatComponent } from './sell-chat/sell-chat.component';
import { SellingComponent } from './selling/selling.component';


@NgModule({
  declarations: [SellerComponent, SellerhomeComponent, ProfileComponent, SellPropertyComponent, SellChatComponent, SellingComponent],
  imports: [
    CommonModule,
    SellerRoutingModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    SliderModule,
    AngularFontAwesomeModule,
    CarouselModule.forRoot()
  ]
})
export class SellerModule { }
