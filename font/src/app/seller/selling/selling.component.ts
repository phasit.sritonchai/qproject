import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { ServerService } from 'src/app/@service/server.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SessionService } from 'src/app/@service/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-selling',
  templateUrl: './selling.component.html',
  styleUrls: ['./selling.component.scss']
})
export class SellingComponent implements OnInit {
  isLinear = false;
  // firstFormGroup: FormGroup;
  // secondFormGroup: FormGroup;

  public firstFormGroup = new FormGroup({
    firstCtrl1: new FormControl(''),
    firstCtrl2: new FormControl(''),
    firstCtrl3: new FormControl(''),
    firstCtrl4: new FormControl('')
  })

  public secondFormGroup = new FormGroup({
    secondCtrl1: new FormControl(''),
    secondCtrl2: new FormControl(''),
    secondCtrl3: new FormControl(''),
    secondCtrl4: new FormControl('')
  })
 
  checked = false;
  indeterminate = false;

  constructor(
    private service: ServerService,
    private modalService: NgbModal,
    private session: SessionService,
    private _formBuilder: FormBuilder,
    private router:Router,
  ) { }

  ngOnInit() {

    this.firstFormGroup = this._formBuilder.group({
      firstCtrl1: ['', Validators.required],
      firstCtrl2: ['', Validators.required],
      firstCtrl3: ['', Validators.required],
      firstCtrl4: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl1: ['', Validators.required],
      secondCtrl2: ['', Validators.required],
      secondCtrl3: ['', Validators.required],
      secondCtrl4: ['', Validators.required],
      secondCtrl5: ['', Validators.required]


    });
  }
  

}
